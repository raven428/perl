#!/usr/bin/perl -w
use strict;
use XML::Bare;

my $ob = new XML::Bare( file => 'pwman.xml' );
my $root = $ob->parse();


print q~<?xml version="1.0" encoding="utf-8" standalone="yes"?>
<KeePassFile>
 <Root>
  <Group>
   <Name>root</Name>




~;

sub snvl {
 my $s = shift();
 return defined($s) ? $s : '';
}

sub process_item {
 my $item = shift();
 print q~<Entry>
 <String>
  <Key>Title</Key>
  <Value>~ . snvl($item->{name}->{value}) . q~</Value>
 </String>
 <String>
  <Key>UserName</Key>
  <Value>~ . snvl($item->{user}->{value}) . q~</Value>
 </String>
 <String>
  <Key>Password</Key>
  <Value>~ . snvl($item->{passwd}->{value}) . q~</Value>
 </String>
 <String>
  <Key>URL</Key>
  <Value>~ . snvl($item->{host}->{value}) . q~</Value>
 </String>
 <String>
  <Key>Notes</Key>
  <Value>~ . snvl($item->{launch}->{value}) . q~</Value>
 </String>
</Entry>~;
}

sub process_list {
 my $list = shift();
 print "<Group>\n";
 print '<Name>' . $list->{name}->{value} . "</Name>\n";
 if(defined($list->{PwList})) {
  if(ref($list->{PwList}) eq 'ARRAY') {
   foreach my $i(@{$list->{PwList}}) {
    process_list($i);
   }
  } else {
   process_list($list->{PwList});
  };
 };
 if(defined($list->{PwItem})) {
  if(ref($list->{PwItem}) eq 'ARRAY') {
   foreach my $i(@{$list->{PwItem}}) {
    process_item($i);
   }
  } else {
   process_item($list->{PwItem});
  };
 };
 print "</Group>\n";
};

process_list($root->{PWMan_PasswordList}->{PwList});

print q~



  </Group>
 </Root>
</KeePassFile>
~;